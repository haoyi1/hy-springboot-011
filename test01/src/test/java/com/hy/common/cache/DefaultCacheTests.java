package com.hy.common.cache;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
/*
* 使用springbootTest注解描述的类
* 为springboot工程的单元测试类
* */
@SpringBootTest
public class DefaultCacheTests {
    /*@Autowired注解描述的属性用spring为其赋值(DI)*/
    @Autowired
    private DefaultCache defaultCache;
    @Test
    public void default01(){
        System.out.println(defaultCache.toString());
        //当输出的值为空null/*
        // 1）属性赋值（自己没有赋值，也没有让spring赋值）
        // 2）属性在的类没有使用注解@SpringBootTest
        // 3）@Test包引入不正确*/
    }
}
